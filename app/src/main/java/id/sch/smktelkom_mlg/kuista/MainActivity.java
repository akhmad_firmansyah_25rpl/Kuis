package id.sch.smktelkom_mlg.kuista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static int hasil, benar, salah;
    TextView pertanyaan;
    RadioGroup rg;
    RadioButton PilihanA, PilihanB, PilihanC, PilihanD;
    int nomor = 0;
    //Pertanyaan
    String[] pertanyaan_kuis = new String[]{
            "1. Sintaks java untuk melakukan kompilasi terhadap berkas program adalah",
            "2. Diantara perintah untuk mencetak berikut, yang benar adalah",
            "3. Stream yang berguna untuk mengirim keluaran ke layar adalah",
            "4. Hasil kompilasi dari berkas java adalah",
            "5. Fungsi method System.in dalam java adalah"
    };

    //Option Jawaban
    String[] pilihan_jawaban = new String[]{
            "Javac", "Java", "Javax", "Java Class",
            "System.Out.Println(Mid Java)", "System.out.Println(Mid Java)", "System.out.println(Mid Java)", "System.Out.println(Mid Java)",
            "System.in", "System.out", "System.err", "System.exit",
            "File executable", "File class", "File BAK", "File Bytecode",
            "Mengirim keluaran layar", "Menangani pembaca dari keyboard", "Menampilkan pesan kesalahan", "Menangani suatu objek"
    };

    //Jawaban
    String[] jawaban_benar = new String[]{
            "Javac",
            "System.out.println(Mid Java)",
            "System.out",
            "File Bytecode",
            "Menangani pembaca dari keyboard"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pertanyaan = findViewById(R.id.pertanyaan);
        rg = findViewById(R.id.radio_group);
        PilihanA = findViewById(R.id.pilihanA);
        PilihanB = findViewById(R.id.pilihanB);
        PilihanC = findViewById(R.id.pilihanC);
        PilihanD = findViewById(R.id.pilihanD);

        pertanyaan.setText(pertanyaan_kuis[nomor]);
        PilihanA.setText(pilihan_jawaban[0]);
        PilihanB.setText(pilihan_jawaban[1]);
        PilihanC.setText(pilihan_jawaban[2]);
        PilihanD.setText(pilihan_jawaban[3]);

        rg.check(0);
        benar = 0;
        salah = 0;
    }

    public void next(View view) {
        if (PilihanA.isChecked() || PilihanB.isChecked() || PilihanC.isChecked() || PilihanD.isChecked()) {
            RadioButton jawaban_user = findViewById(rg.getCheckedRadioButtonId());
            String ambil_jawaban_user = jawaban_user.getText().toString();
            rg.check(0);
            if (ambil_jawaban_user.equalsIgnoreCase(jawaban_benar[nomor])) benar++;
            else salah++;
            nomor++;
            if (nomor < pertanyaan_kuis.length) {
                pertanyaan.setText(pertanyaan_kuis[nomor]);
                PilihanA.setText(pilihan_jawaban[(nomor * 4) + 0]);
                PilihanB.setText(pilihan_jawaban[(nomor * 4) + 1]);
                PilihanC.setText(pilihan_jawaban[(nomor * 4) + 2]);
                PilihanD.setText(pilihan_jawaban[(nomor * 4) + 3]);

            } else {
                hasil = benar * 20;
                Intent selesai = new Intent(getApplicationContext(), HasilKuis.class);
                startActivity(selesai);
            }
        } else {
            Toast.makeText(this, "Jawaban belum di isi!", Toast.LENGTH_SHORT).show();
        }
    }
}
